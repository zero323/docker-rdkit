#! /usr/bin/env bash

conda-sign --verify /home/rdkit/miniconda3/pkgs/{rdkit,boost}*tar.bz2 | grep ERROR

if [ $? -eq 0 ];
	then exit 123
fi


