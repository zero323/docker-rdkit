Docker - RDKit
=================

[RDkit](http://www.rdkit.org/) images created using [RDKit Conda recipes](https://github.com/rdkit/).

Interactive usage with Jupyter notebook:

```bash
docker pull zero323/rdkit:jupyter

# Create data container.
# See: http://www.tech-d.net/2013/12/16/persistent-volumes-with-docker-container-as-volume-pattern/
docker run --name rdkit_notebooks -v /data/notebooks busybox sh

# Run container and expose port 8888 on the localhost
docker run -p 8888:8888 -d --volumes-from rdkit_notebooks zero323/rdkit:jupyter
```
